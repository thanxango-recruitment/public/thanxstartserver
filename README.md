# Hello

Welcome to the Thanxngo recruitment exercice!

The goal of this exercice is to create a very simple python server
that serves a REST API for consulting and listing people's profiles.

The idea is to have something like this demo server:
https://agile-beach-76323.herokuapp.com/api/

We already have setup a django app and a flask app.

You only need to pick one and complete the code but if you want to use
another framework feel free to do so.

# Setup

We suggest you to use a dedicated virtualenv for this exercice, the
easiest way to get started is to use
[pipenv](https://github.com/pypa/pipenv).

## Django setup

To run the django server with pipenv, `cd` into the `djangoapp`
directory and run the following commands :

```
$ pipenv install
$ pipenv run python manage.py runserver
```

You should have a working testing server at http://localhost:8000

We gonna use a **SQLite** database already prepared and the [Django
ORM](https://docs.djangoproject.com/en/2.1/ref/models/querysets/).

## Flask setup

To run the flask server with pipenv, `cd` into the `flask`
directory and run the following commands :

```
$ pipenv install
$ pipenv run python app.py
```

You should have a working testing server at http://localhost:5000

We gonna use a **SQLite** database already prepared and the [Flask -
SQLAlchemy ORM](http://flask-sqlalchemy.pocoo.org/2.3/)


# Exercice

Here is the thing you need to do:

# 1. Load the data into a sqlite database

We have a list of people in the `data.json` file.

A person have:
- a `code` (string)
- a `first name` (string)
- a `last_name` (string)
- a `picture` (link)

We want to load the content of `data.json` in a **SQLite** database.  We
already have written a `load_data.py` file for Django and Flask, you
need to write the `create_people()` and `load_data()` functions.

We should be able to load data from `data.json` to a **SQLite**
database with the command:

```
$ pipenv run python load_data.py
```

# 2. List all Users in a JSON response

Now that the database contains a list of users we want to be able to list people on the

- http://localhost:8000/api/people/ url for Django
- http://localhost:5000/api/people/ url for Flask

The view should already render an empty JSON list.

To edit the content of this view, write the code in:
- The method `list_view` in `djangoapp/people/views.py` for Django
- The method `list_view` in `flask/app.py` for Flask

We expect the view to render the list:
```
[
  {
    "first_name": "April",
    "last_name": "Clark",
    "code": "5aefd18e",
    "picture": "https://agile-beach-76323.herokuapp.com/picture/dacd1cf2"
  },
  {
    "first_name": "Timothy",
    "last_name": "Wilson",
    "code": "78a0ab69",
    "picture": "https://agile-beach-76323.herokuapp.com/picture/13471398"
  },
  ...
]
```

# 3. Filter users by code

The next step is to be able to search people by code using a url like
`/api/people/?code=5aefd18e`.


We already have prepared the `list_view` methods to fetch the `code`
parameter, use it to filter the results.

For the request `/api/people/?code=5aefd18e` We expect the view to render the list:
```
[
  {
    "first_name": "April",
    "last_name": "Clark",
    "code": "5aefd18e",
    "picture": "https://agile-beach-76323.herokuapp.com/picture/dacd1cf2"
  }
]
```

# 4. Search by first_name or last_name

The final step is to search people by their `first_name` or `last_name`.

As for the `code` parameter, we already have prepared the `list_view`
methods to fetch the `search` parameter, use it to filter the results.

For the request `/api/people/?search=April` We expect the view to render the list:
```
[
  {
    "first_name": "April",
    "last_name": "Clark",
    "code": "5aefd18e",
    "picture": "https://agile-beach-76323.herokuapp.com/picture/dacd1cf2"
  }
]
```

If you have time you can also make the search more friendly by
allowing partial lookup. For example the request
`/api/people/?search=apri` should return the same result as the previous one.
