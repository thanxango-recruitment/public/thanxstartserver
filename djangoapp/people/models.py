from django.db import models


class People(models.Model):
    code = models.CharField(max_length=36, unique=True, blank=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    picture = models.URLField(blank=True)
