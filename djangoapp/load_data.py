import django
import json
import os

# Don't worry about that, we just initialize django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangoapp.settings")
django.setup()

from people.models import People

JSON_FILE = "../data.json"


def create_people(people):
    """
    Create a People object in the database.
    Use the People.objects.create() method
    See the documentation here:
    https://docs.djangoproject.com/en/2.0/topics/db/queries/#creating-objects
    """
    # Code here


def load_data():
    """
    Load json data into sqlite.db.

    Use the create_people() method to save in the database each object.
    """
    with open(JSON_FILE, "r") as fo:
        data = json.load(fo)

    # Code here


if __name__ == "__main__":
    load_data()
