from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
db = SQLAlchemy(app)


class People(db.Model):
    __tablename__ = "people"
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(10), unique=True)
    first_name = db.Column(db.String(120), nullable=False)
    last_name = db.Column(db.String(120), nullable=False)
    picture = db.Column(db.String(120), nullable=False)

    def __init__(self, id, code, first_name, last_name, picture):
        self.id = id
        self.code = code
        self.first_name = first_name
        self.last_name = last_name
        self.picture = picture

    def __repr__(self):
        return "<People {}>".format(self.id)


@app.route("/")
def entry_point():
    return "Hello World!"


@app.route("/api/people/")
def list_view():
    code = request.args.get("code")
    search = request.args.get("search")
    data = []

    # code here
    # ...

    return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True)
