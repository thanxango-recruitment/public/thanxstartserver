"""Script to load json data to sqlite db."""
import json
from app import db, People

JSON_FILE = "../data.json"


def create_people(people):
    """
    Create a People object and add it to the database.
    Use the People() class and the db.session.add() method.

    You can find documentation about sqlalchemy on this url
    http://flask-sqlalchemy.pocoo.org/2.3/queries/
    """
    # Code here


def load_data():
    """
    Load json data into sqlite.db.

    Use the create_people() method and don't forget to commit changes with
    db.session.commit() at the end.

    """
    with open(JSON_FILE, "r") as fo:
        data = json.load(fo)

    # Code here


if __name__ == "__main__":
    load_data()
